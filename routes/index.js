var express = require('express');
var router = express.Router();
var passport=require('passport');

var fixtureData = require('./fixture_data.json');


var isAuthenticated = function (req, res, next) {
  // if user is authenticated in the session, call the next() to call the next request handler
  // Passport adds this method to request object. A middleware is allowed to add properties to
  // request and response objects
  if (req.isAuthenticated())
    return next();
  // if the user is not authenticated then redirect him to the login page
  res.redirect('/homePage');
}

module.exports = function(passport){

  /* GET login page. */
  router.get('/', function(req, res) {
    // Display the Login page with any flash message, if any
    res.render('index', {
      message: req.flash('message') ,
      isAuthenticated:req.isAuthenticated(),
      user:req.user});
  });

  /* GET login Page */
  router.get('/signin', function(req, res){
    console.log('get login!');
    res.render('signin',{message: req.flash('message')});
  });

  /* Handle Signin POST */
  router.post('/signin', passport.authenticate('signin', {

    successRedirect: '/homePage',
    failureRedirect: '/signin',
    failureFlash : true

  }));



  /* GET Registration Page */
  router.get('/signup', function(req, res){
    res.render('signup',{message: req.flash('message')});
  });

  /* Handle Registration POST */
  router.post('/signup', passport.authenticate('signup', {
    successRedirect: '/homePage',
    failureRedirect: '/signup',
    failureFlash : true
  }));

  /* GET Home Page */
  router.get('/homePage', isAuthenticated, function(req, res){
    res.render('homePage', { user: req.user ,
      fixtureData: fixtureData
    });
  });

  /* Handle Logout */
  router.get('/signout', function(req, res) {
    req.logout();
    res.redirect('/');
  });



  return router;
}