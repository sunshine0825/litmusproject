This is a code challenger for Litmus.

To run the code, please 
1. clone the code to your local machine
2. In a shell, director to you local code repository
3. run npm init
4. Open your local Mongodb
5. Create a database in mongodb with name "test"
6. you may need to adjust mongodb location in the "db.js" accordingly
7. Run "www" in "bin" directory 
